window.addEventListener('DOMContentLoaded', async () => {

    const locationUrl = 'http://localhost:8000/api/locations/';
    const stateResponse = await fetch(locationUrl);

    if (stateResponse.ok) {
        const data= await stateResponse.json();
        const selectTag = document.getElementById('location')
        for(let location of data.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
        }
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const dataObject = Object.fromEntries(formData);

    const fetchOptions = {
        method: "POST",
        body: JSON.stringify(dataObject),
        headers: {
        'Content-Type': 'application/json',
        },
    };
    
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const response = await fetch(conferenceUrl, fetchOptions);

    if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
    }
    });
    });
    