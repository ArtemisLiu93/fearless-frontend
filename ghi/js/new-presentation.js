window.addEventListener('DOMContentLoaded', async () => {

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const conferenceResponse = await fetch(conferenceUrl);


    if (conferenceResponse.ok) {
        const data = await conferenceResponse.json();
        const selectTag = document.getElementById('conference')
        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.id;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
    }
    const formTag = document.getElementById('create-presentation-form;');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const dataObject = Object.fromEntries(formData);

    const fetchOptions = {
        method: "POST",
        body: JSON.stringify(dataObject),
        headers: {
            'Content-Type': 'application/json',
        },
    }

    const presentationUrl = ''
    })
});


