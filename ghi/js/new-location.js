//! This line adds an event listener for the DOMContentLoaded event, which fires when the initial HTML document has been completely loaded and parsed. It triggers the anonymous async arrow function as the event handler.
window.addEventListener('DOMContentLoaded', async () => {
    
    //!These lines declare a constant url that holds the URL of the API endpoint. Then, using the fetch() function, an HTTP GET request is sent to the specified URL. The response is stored in the stateResponse variable. The await keyword is used to pause the execution of the code until the promise returned by fetch() is resolved.
    const url = 'http://localhost:8000/api/states/';
    const stateResponse = await fetch(url);
    console.log(stateResponse);
    //!This if statement checks if the response from the API is successful (status code 200-299). If the response is okay, the json() method is called on the stateResponse object to parse the response body as JSON. The resulting JSON data is stored in the data variable using await to ensure the promise is resolved.
    if (stateResponse.ok) {
        const data= await stateResponse.json();
        console.log(data);
        //!This line retrieves the DOM element with the ID 'state' using getElementById(). It assumes there is an HTML element, most likely a <select> element, with the ID 'state' in the loaded document.
        const selectTag = document.getElementById('state')
    

        //! This for...of loop iterates over each state object in the data.states array obtained from the API response. For each state, it creates a new <option> element using createElement('option'). The value property of the option is set to the state's abbreviation, and the innerHTML is set to the state's name. Finally, the option element is appended as a child to the selectTag element, which is the select element in the HTML. The overall effect of the code is to fetch data from the specified API endpoint, parse it as JSON, and dynamically populate the select element with options based on the retrieved state data.
        for(let state of data.states) {
            const option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);
        }
    }   

        //! This line selects the form element with the ID 'create-location-form' and stores it in the formTag constant.
        const formTag = document.getElementById('create-location-form');

        //! This line adds an event listener to the form element. It listens for the 'submit' event, which occurs when the form is submitted. The second argument is an anonymous async arrow function that serves as the event handler.
        formTag.addEventListener('submit', async event => {

        //! This line prevents the default behavior of the form submission. It stops the form from being submitted and the page from refreshing.
        event.preventDefault();
        
        //! This line creates a new FormData object, which represents the form data. It takes the formTag as the argument to extract the form data from the HTML form.
        const formData = new FormData(formTag);

        //! This line converts the formData into a plain JavaScript object using the Object.fromEntries() method. It transforms the form data into key-value pairs.
        const dataObject = Object.fromEntries(formData);
        
        //! This block of code defines the fetchOptions object. It specifies the HTTP method as POST, includes the serialized dataObject as the request body by converting it to a JSON string using JSON.stringify(), and sets the 'Content-Type' header to 'application/json'.
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(dataObject),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        
        //! This line declares the locationUrl constant, which holds the URL where the form data will be sent using the HTTP POST method.
        const locationUrl = 'http://localhost:8000/api/locations/';

        //! This line sends an asynchronous POST request to the specified locationUrl with the fetchOptions as configuration. It uses the await keyword to pause the execution and wait for the response to be received.
        const response = await fetch(locationUrl, fetchOptions);

        //!This if statement checks if the response received is successful (status code 200-299). If it is, the formTag is reset, clearing the form inputs, and the response body is parsed as JSON using response.json(). The resulting JSON data is stored in the newLocation constant.
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }
        });
        });