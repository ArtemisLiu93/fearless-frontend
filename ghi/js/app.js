function createCard(name, description, pictureUrl, startDate, endDate, subName) {
    return `
    <div class="col">
    <div class="card h-100 shadow p-3 mb-5">
      <img src=${pictureUrl} class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${subName}</h6>
        <p class="card-text">${description}</p>
        </div>
        <div class="card-footer"><small class="text-body-secondary">${String(startDate.getMonth()+1)}/${String(startDate.getDay()+24)}/${String(startDate.getFullYear())}
        - ${String(endDate.getMonth()+1)}/${String(endDate.getDay()+24)}/${String(endDate.getFullYear())}</small></div>
      </div>
    </div>
      `;
  }
  function errorMessage() {
    return `<div class="alert alert-danger" role="alert">
    Error!
  </div>`
  }
  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        const html = errorMessage();
        const column = document.querySelector('.row');
        column.innerHTML = html;
      } else {
        const data = await response.json();
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts);
            const subName = details.conference.location.name;
            const endDate = new Date(details.conference.ends);
            const html = createCard(title, description, pictureUrl, startDate, endDate, subName);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }
      }
    } catch (e) {
      console.error('An error occurred:', e);
      // Perform additional error handling steps if needed
    }
  });

  
  
  
  
  
  
  
  

  
  
