window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const createAttendeeForm = document.getElementById('create-attendee-form')


    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    console.log(response)
    if (response.ok) {
      const data = await response.json();
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      const loadingIcon = document.getElementById('loading-conference-spinner');
      loadingIcon.classList.add('d-none');
      selectTag.classList.remove('d-none');

      createAttendeeForm.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(createAttendeeForm);
        const formEntries = Object.fromEntries(formData.entries());

        const options = {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formEntries),
        };
        try {
            const response = await fetch('http://localhost:8001/api/attendees/', options);
            if (response.ok) {
                console.log('Attendee submitted successfully');
            } else {
                console.log('An error occurred;', error);
            }
        } catch (error) {
            console.log('An error occurred;', error);
        }

        const message = document.getElementById('success-message');
        message.classList.remove('d-none');
        createAttendeeForm.classList.add('d-none');
      });
    }
  });
