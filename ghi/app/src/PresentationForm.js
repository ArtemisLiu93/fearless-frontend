import React, { useEffect, useState } from 'react';


function PresentationForm() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [conference, setConference] = useState('');
    const [conferences, setConferences] = useState([]);
    const [synopsis, setSynopsis] = useState('');

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
    
        const response = await fetch(url);
    
        if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
    }
    };

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }
    
    const nameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const emailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    };

    const companyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const titleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const synopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;

        const conferenceUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();

            setName('');
            setEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    };

    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new Presentation</h1>
            <form id="create-presentation-form" onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                <input value={name} onChange={nameChange} placeholder="Presenter_name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter's Name</label>
                </div>
                <div className="form-floating mb-3">
                <input value={email} onChange={emailChange} placeholder="Presenter_email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter's Email </label>
                </div>
                <div className="form-floating mb-3">
                <input value={companyName} onChange={companyNameChange} placeholder="Company_name" required type="text" id="company_name" name="company_name" className="form-control" />
                <label htmlFor="company_name">Company's Name</label>
                </div>
                <div className="form-floating mb-3">
                <input value={title} onChange={titleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                <label htmlFor="title">Title</label>
                </div>
                <div value={synopsis} onChange={synopsisChange} className ="mb-3"><label htmlFor = "synopsis" className="form-label"> Synopsis </label>
                <textarea className="form-control" id="synopsis" name="synopsis" rows="3"></textarea>
                </div>

                <div className="mb-3">
                <select required onChange={handleConferenceChange} value={conference} className="form-select" name="conference">
                    <option value="">Choose a conference</option>
                    {conferences.map ((conference, index) => {
                        return (
                            <option value={conference.id} key={index}>
                            {conference.name}
                        </option>
                        );
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
    }
    export default PresentationForm;