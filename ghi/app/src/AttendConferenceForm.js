import React, { useEffect, useState } from 'react';

function AttendConferenceForm() {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [isSignedUp, setIsSignedUp] = useState(false);
  const [conference, setConference] = useState('');
  const [conferences, setConferences] = useState([]);


  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };


  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsSignedUp(true);

  }

  const handleConference = (event) => {
    const value = event.target.value;
    setConference(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (conferences.length > 0) {
      spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
      dropdownClasses = 'form-select';
    }
    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (isSignedUp) {
      messageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    } 
    console.log(spinnerClasses)
  return (
      <div className="my-5 container">
        <div className="row">
          <div className="col col-sm-auto">
            <img
              width="300"
              className="bg-white rounded shadow d-block mx-auto mb-4"
              src="/logo.svg"
              alt="Logo"
            />
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form id="create-attendee-form" className={formClasses}>
                  <h1 className="card-title">It's Conference Time!</h1>
                  <p className="mb-3">
                    Please choose which conference you'd like to attend.
                  </p>
                  <div className={spinnerClasses} id="loading-conference-spinner">
                    <div className="spinner-grow text-secondary" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                  <div className="mb-3">
                    <select required id="conference" className={dropdownClasses} name="conference" value={conference} onChange={handleConference}>
                      <option value="">Choose a conference</option>
                      {conferences.map((conference, index) => {
                        return (
                        <option key={index} value={conference.id}>
                          {conference.name}
                        </option>
                        )
                      })}
                    </select>
                  </div>
                  <p className="mb-3">Now, tell us about yourself.</p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input
                          required
                          placeholder="Your full name"
                          type="text"
                          id="name"
                          name="name"
                          className="form-control"
                          value={name}
                          onChange={handleNameChange}
                        />
                        <label htmlFor="name">Your full name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input
                          required
                          placeholder="Your email address"
                          type="email"
                          id="email"
                          name="email"
                          className="form-control"
                          value={email}
                          onChange={handleEmailChange}
                        />
                        <label htmlFor="email">Your email address</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">
                    I'm going!
                  </button>
                    <div className={messageClasses} role="alert">
                      Congratulations! You're all signed up!
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  
  export default AttendConferenceForm;