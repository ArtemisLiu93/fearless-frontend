import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
        }
    };

    const nameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const startsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    };

    const endsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    };

    const descriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    };

    const maxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    };

    const maxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    };

    const locationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);
        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
    }







return (
    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
        <h1>Create a new conference</h1>
        <form id="create-conference-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
            <input
                value={name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
                onChange={nameChange}
            />
            <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
            <input
                value={starts}
                placeholder="Starts"
                required
                type="date"
                name="starts"
                id="starts"
                className="form-control"
                onChange={startsChange}
            />
            <label htmlFor="starts">Starts </label>
            </div>
            <div className="form-floating mb-3">
            <input
                value={ends}
                placeholder="ends"
                required
                type="date"
                id="ends"
                name="ends"
                className="form-control"
                onChange={endsChange}
            />
            <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
            <label htmlFor="description" className="form-label">
                Description
            </label>
            <textarea
                value={description}
                className="form-control"
                id="description"
                name="description"
                rows="3"
                onChange= {descriptionChange}
            ></textarea>
            </div>
            <div className="form-floating mb-3">
            <input
                value={maxPresentations}
                placeholder="Name"
                required
                type="text"
                name="max_presentations"
                id="max_presentations"
                className="form-control"
                onChange={maxPresentationsChange}
            />
            <label htmlFor="name">Maximum Presentations</label>
            </div>
            <div className="form-floating mb-3">
            <input
                value={maxAttendees}
                placeholder="Name"
                required
                type="text"
                name="max_attendees"
                id="max_attendees"
                className="form-control"
                onChange={maxAttendeesChange}
            />
            <label htmlFor="name">Max Attendees</label>
            </div>

            <div className="mb-3">
            <select
                value={location}
                required
                id="location"
                className="form-select"
                name="location"
                onChange={locationChange}
            >
                <option value="">
                Choose a location
                </option>
                {locations.map(location => {
                    return (
                        <option value={location.id} key={location.id}>
                            {location.name}
                        </option>
                    );
                })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
        </form>
        </div>
    </div>
    </div>
);
}

export default ConferenceForm;