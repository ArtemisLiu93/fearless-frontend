import Nav from './Nav';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm'
import AttendConferenceForm from './AttendConferenceForm';
import AttendeesList from './AttendeesList'
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css"; 


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav className= "Navbar" />
        <Routes>
          <Route path= "locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path= "conferences">
            <Route path="new" element= {<ConferenceForm />} />
          </Route>
          <Route path= "attendees">
            <Route path="new" element= {<AttendConferenceForm />} />
            <Route path="" element= {<AttendeesList attendees = {props.attendees}/>} />
          </Route>
          <Route path= "presentations">
            <Route path="new" element= {<PresentationForm />} />
          </Route>
          <Route index element={<MainPage />} />
        </Routes>
    </BrowserRouter>
  );
}

export default App;